<?php

namespace Jump\Cli\Commands;

interface ICommand
{
    public function help(): void;
}
