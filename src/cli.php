<?php

use DI\ContainerBuilder;
use Jump\Cli\CommandProcessor;

session_start();
date_default_timezone_set('America/Chicago');

require_once(__DIR__ . '/../vendor/autoload.php');

try {
    $arguments = $argv;
    $containerBuilder = new ContainerBuilder;

    // Set up other things in Dependency Injection, such as Database connection, etc.
    // $containerBuilder->addDefinitions([
    //]);

    $container = $containerBuilder->build();
    /** @var CommandProcessor $cli */
    $cli = $container->get(CommandProcessor::class);


    // Register CLI Commands
    //$cli->registerCommand('test', TestCommand::class);


    $cli->runCommand($arguments);
}
catch (Exception $ex) {
    echo get_class($ex) . "\r\n";
    echo $ex->getMessage();
    echo "\r\n\n";
}
