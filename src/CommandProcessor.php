<?php

namespace Jump\Cli;

use DI\DependencyException;
use DI\FactoryInterface;
use DI\NotFoundException;
use Jump\Cli\Commands\ICommand;
use Jump\Cli\Exceptions\CliException;
use Jump\Cli\Exceptions\CommandNotFoundException;
use Jump\Cli\Exceptions\InvalidCommandClassException;

class CommandProcessor
{
    protected FactoryInterface $container;
    protected array $registeredCommands;

    public function __construct(FactoryInterface $container) {
        $this->container = $container;
        $this->registeredCommands = [];
    }

    /**
     * Registers a CLI command with a handler class
     * @param string $commandName
     * @param string $className
     */
    public function registerCommand(string $commandName, string $className): void {
        $this->registeredCommands[strtolower($commandName)] = $className;
    }

    /**
     * Runs a command from the arguments passed
     * @param array $args
     * @throws DependencyException
     * @throws NotFoundException
     */
    public function runCommand(array $args) {
        try {
            $cmd = $this->parseCommand($args);
            if ($cmd->handlerName == "root") {
                $this->help();
            } else {
                $cmdClass = $this->createCommandClass($cmd);
                $this->issueCommand($cmd, $cmdClass);
            }
        }
        catch (CliException $ex) {
            echo $ex->getMessage();
            echo "\r\n\n";
        }
    }

    /**
     * Creates an instance of the command handler class (must implement ICommand)
     * @param CommandArguments $cmd
     * @return ICommand|null
     * @throws InvalidCommandClassException
     * @throws DependencyException
     * @throws NotFoundException
     */
    protected function createCommandClass(CommandArguments $cmd): ?ICommand {
        if (class_exists($cmd->handlerName)) {
            $command = $this->container->make($cmd->handlerName);
            if (!($command instanceof ICommand)) {
                throw new InvalidCommandClassException("$cmd->handlerName is not a valid command class.  Commands must implement ICommand.");
            }
            return $command;
        }
        else
            return null;
    }

    /**
     * Displays an error message
     * @param CommandArguments $cmd
     * @param string $msg
     */
    protected function displayError(CommandArguments $cmd, string $msg): void {
        echo "Error in command <$cmd->handlerName:$cmd->commandName>: $msg";
    }

    /**
     * Displays some help information
     */
    protected function help(): void {
        echo "Jump Command-Line Interface";
    }

    /**
     * Issues the command
     * @param CommandArguments $cmd
     * @param ICommand $cmdClass
     */
    protected function issueCommand(CommandArguments $cmd, ICommand $cmdClass): void {
        $cmdMethod = $cmd->commandName;
        if (method_exists($cmdClass, $cmdMethod)) {
            $cmdClass->$cmdMethod($cmd->arguments, $cmd->options);
        }
        else {
            if ($cmd->commandName == "run")
                $this->displayError($cmd, "No such command: $cmd->handlerName");
            else
                $this->displayError($cmd, "No such command: $cmd->handlerName:$cmd->commandName");
        }
        echo "\r\n\n";
    }

    /**
     * Parses the CommandArguments from the arguments passed
     * @param array $args
     * @return CommandArguments
     * @throws CommandNotFoundException
     */
    protected function parseCommand(array $args): CommandArguments {
        $cmd = new CommandArguments();
        $argCount = count($args);

        if ($argCount < 2) {
            $cmd->handlerName = "root";
            $cmd->commandName = "run";
            $cmd->options["help"] = "";
        }
        else {
            $commandPieces = explode(":", $args[1]);
            $commandName = strtolower($commandPieces[0]);
            if (isset($this->registeredCommands[$commandName]))
                $cmd->handlerName = $this->registeredCommands[$commandName];
            else {
                throw new CommandNotFoundException("The command $commandName was not found.");
            }
            if (count($commandPieces) > 1) {
                $cmd->commandName = strtolower($commandPieces[1]);
            }
            else {
                $cmd->commandName = "run";
            }
            for ($i = 2; $i < $argCount; $i++) {
                if (substr($args[$i], 0, 2) == "--") {
                    $optString = substr($args[$i], 2);
                    if (strlen($optString) > 0) {
                        $optPieces = explode("=", $optString, 2);
                        if (count($optPieces) > 1) {
                            $cmd->options[strtolower($optPieces[0])] = $optPieces[1];
                        }
                        else {
                            $cmd->options[strtolower($optPieces[0])] = "";
                        }
                    }
                }
                else {
                    array_push($cmd->arguments, $args[$i]);
                }
            }
        }
        return $cmd;
    }
}
