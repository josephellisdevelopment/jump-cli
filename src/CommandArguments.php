<?php

namespace Jump\Cli;

class CommandArguments
{
    public string $handlerName;
    public ?string $commandName;
    public array $arguments;
    public array $options;
    public string $error;

    public function __construct() {
        $this->handlerName = "";
        $this->commandName = null;
        $this->arguments = array();
        $this->options = array();
        $this->error = "";
    }
}
